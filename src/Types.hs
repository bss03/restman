module Types
    ( -- * Global Types for TUI manipulation
      AppS(..)
    , OverlayS(..)
      -- * Range Ord-like types and helpers
    , RangeInAlign(..)
    , VertAlign(..)
    , WideAlign(..)
    , left
    , top
    ) where


-- brick
import Brick.Focus (FocusRing)
import Brick.Types (EventM, Extent)
import Brick.Widgets.Edit (Editor)
import Brick.Widgets.List (List)

-- bytestring
import qualified Data.ByteString.Lazy as Lazy (ByteString)
-- text
import Data.Text (Text)

-- internal imports
import HTTP.Client (Header, Method)

{-|
Application State

Single URL editor widget.

Widget names are all 'Text' values.
-}
data AppS = AppS
  { focus :: FocusRing Text -- ^ Focus tracking within the main interface.
  , methodEditor :: Editor Method Text -- ^ text entry for the HTTP method to use for request
  , overlayState :: Maybe OverlayS -- ^ If not Nothing, the overlay is displayed with this state
  , urlEditor :: !(Editor String Text) -- ^ text entry for URL to request
  , lastResponse :: !Lazy.ByteString -- ^ Either the response body, or an error message
  , useDefaultHeaders :: Bool -- ^ If False, replace with; otherwise append custom headers
  , customHeaders :: [Header] -- ^ additional/replacement request headers
  , payload :: Maybe Lazy.ByteString -- ^ just the request body, no content type / filename
  }

{-|
State of the UI overlay, which is currently just the "pop-up" for the method selector.
-}
data OverlayS = OverlayS
  { methodEditorExtent :: Extent Text -- ^ Where the pop-up should start
  , methodList :: List Text Method -- ^ Method selection
  }


-- | Alignment of one range strictly within another range both over a discete, totally ordered set.
data RangeInAlign
  = Min -- ^ min of both ranges match
  | MidL -- ^ inner midpoint as close as possible to, but less than or equal to outer midpoint
  | MidG -- ^ inner midpoint as close as possible to, but greater than or equal to outer midpoint
  | Max -- ^ max of both ranges match

-- | Vertical alignment
type VertAlign = RangeInAlign

top{-, centerHigh, centerLow, bottom-} :: VertAlign

-- | Align at the top, matching min vert corodinates.
top = Min
{-
centerHigh = MidL
centerLow = MidG
bottom = Max
-}

-- | Wide-ways ("horizonal") alignment
type WideAlign = RangeInAlign
left{-, centerLeft, centerRight, right-} :: VertAlign

-- | Align on the left, matching min wide coordinates.
left = Min
{-
centerLeft = MidL
centerRight = MidG
right = Max
-}
